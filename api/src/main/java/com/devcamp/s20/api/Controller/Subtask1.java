package com.devcamp.s20.api.Controller;

import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class Subtask1 {
   //subtask 1
    @GetMapping("/reverse")
    public String reverseString(@RequestParam(name = "inputString", required = true) String input) {
       String newString = new StringBuilder(input).reverse().toString();
       return newString;
    }
    //subtask 2
    @GetMapping("/panlindrome")
    public String isPalindrome(@RequestParam(name = "inputString", required = true) String input) {
       String newString = new StringBuilder(input).reverse().toString();
       if (newString.equals(input)){
        return "This is a panlindrome string!";
       } else {
        return "This is not a panlindrome string!";
       }

    }
    //subtask 3
    @GetMapping("/remove_duplicate")
    public String removeDuplicateChar(@RequestParam(name = "inputString", required = true) String input) {
      char[] charArray = input.toCharArray();
      Set<Character> charSet = new LinkedHashSet<Character>();
       for (char element: charArray){
         charSet.add(element);
       }
       StringBuilder result = new StringBuilder();
       for (Character character: charSet){
         result.append(character);
       }
       return result.toString();
    }
    //subtask 4
    @GetMapping("/concat")
    public String concatString(@RequestParam(name = "inputString1", required = true) String input1, @RequestParam(name = "inputString2", required = true) String input2) {
      String result = new String();
      int len1 = input1.length();
      int len2 = input2.length();
      if (len1==len2){
         result = input1 + input2;
      } else if (len1>len2){
        String subString1 = input1.substring(len1-len2);
        result = subString1 + input2;
      } else if (len1<len2){
         String subString2 = input2.substring(len2-len1);   
         result = input1 + subString2;   
      }
      return result;
}
}
